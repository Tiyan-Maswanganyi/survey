import React, { Component } from 'react';
import Questions from "./../../data/questions.json";
import './Question.css';

export default class Question extends Component {


    constructor(props) {
        super(props);
        this.state = {count: 1,answers:[]};
    }

     GetQuestion(key) {
         if(key === null || undefined) {key = 1};

       const result = Questions.filter((data) => {  if(data.key === key){ console.log ( "data" + data.question + "  passed key "+ key); return data.question; } return ""});
       return result;
     }

     handleChange(data,e) {
         const {value}  = e.target;
         console.log(value);
         console.log(data);
         let {answers} = this.state;
         answers.push({question: data, answer: value})
         this.setState(answers);
         let {count} = this.state;
         if(count < 0 || count > 10){count = 1};
         count = count + 1;
         this.setState({count});
         console.log(this.state);
     }

     change =  (data,e) => {
         const {value } = e.target;
         this.setState({question: data, answer: value});
         console.log(this.state)
     }

     nextQuestion= () => {


        //Save answer to array
        let {answers} = this.state;
        answers.push({question: this.state.question,answer: this.state.answer});
         this.setState({question:'', answer:''});
        
        // Change Question
        let {count} = this.state;
        if(count < 0 || count > 10){count = 1};
        count = count + 1;
        this.setState({count});
        
     }

    render() {
    
        if(this.state.count > 9){
            this.props.history.push('/end');
        }

       console.log(Questions[0]);
        console.log( this.state );
        const q = this.GetQuestion(this.state.count);
        console.log(q[0]);
      

        return (
            <div className="container">
               <h4>{q[0].question !== undefined?  q[0].question: ''}</h4>
               {q[0].type === 'multiplechoice'? q[0].options.map((data)=> { 
                   return <button className ="button"
                                  value={data} key={data} onClick={(e) => this.handleChange(q[0].question,e)}>{data}</button> 
                   }): <div><input type="text" value={this.state.answer} onChange={(e) => this.change(q[0].question,e)} /> <button onClick={this.nextQuestion} className="button">Next</button></div>}
            </div>
        )
    }
}
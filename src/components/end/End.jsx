import React, { Component } from 'react';
import logo from './../../img/HAF_logo.png'
import './End.css';
import  { Link } from "react-router-dom";
class End extends Component {


    render() {
        return (
            <div className="container">
                <img src={logo} alt="" className="center" />
                <h4>Thank you for taking this quick survey it will help us make your time at HAF more enjoyable.</h4>
                    
            </div>
        );
    }
}

export default End;
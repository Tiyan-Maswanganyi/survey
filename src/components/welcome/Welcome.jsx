import React, { Component } from 'react';
import logo from './../../img/HAF_logo.png'
import './Welcome.css';
import  { Link } from "react-router-dom";
class Welcome extends Component {


    render() {
        return (
            <div className="container">
                <img src={logo} alt="" className="center" />
                <h4>Thanks for serving at Hillsong Africa Foundation
                    We'd love to know a bit more about your experience serving.</h4>
                    <div className="box">
                        <Link to="/question" className="btn btn-white btn-animation-1">Give Feedback</Link> 
                    </div>
            </div>
        );
    }
}

export default Welcome;
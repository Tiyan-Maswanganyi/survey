import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import logo from './img/background.jpg';

import './App.css';
import Welcome from './components/welcome/Welcome';
import Question from './components/question/Question';
import End from './components/end/End';

function App() {
  return (
   <Router>
        
           <Route path="/" exact component={Welcome} />
           <Route path="/question" component={Question} />
           <Route path="/end" component={End}/>
   </Router>
  );
}

export default App;
